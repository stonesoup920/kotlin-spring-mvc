package com.stonesoupprogramming.kotlinspringmvc

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod

@SpringBootApplication //This performs magic under the hood to launch a spring web application
class KotlinSpringMvcHelloWorldApplication

//Entry point to the application
fun main(args: Array<String>) {
    SpringApplication.run(KotlinSpringMvcHelloWorldApplication::class.java, *args)
}

//This is a class that we are using for our form
//Kotlin let's us one line it!
data class Registration(var firstName: String = "", var lastName: String = "")

@Controller //Tells Spring this is a controller class
@RequestMapping("/") //Tells Spring to handle web requests at the root
class Controller {

    //This function will handle HTTP Get Requests
    @RequestMapping(method = arrayOf(RequestMethod.GET))
    fun doIndexGet(model: Model): String{
        //Send a new instance of Registration back to the view
        model.addAttribute("registration", Registration())

        //Render the index.html page
        return "index"
    }

    //This method handles HTTP Post
    @RequestMapping(method = arrayOf(RequestMethod.POST))
    fun doIndexPost(formParams: Registration, model: Model): String {
        //Send a greeting message to the view
        model.addAttribute("greet", "Hello ${formParams.firstName} ${formParams.lastName}")

        //Render the greet.html page
        return "greet"
    }

    //This handles GET requests for /greet.html
    @RequestMapping(path = arrayOf("/greet"), method=arrayOf(RequestMethod.GET))
    fun doGreetGet(): String = "greet" //Just tell it to render the greet.html page
}